from application import db, login_manager
import datetime
from flask_login import UserMixin

"""Creating a user decorator function that will get the user by medic id and create a session for that particular user"""


@login_manager.user_loader
def load_user(user_id):
    return Users.query.get(int(user_id))


"""Creating database tables into the database for the different ddata to be collected and analysed in the system"""


class Users(db.Model, UserMixin):
    position = db.Column(db.String(20), nullable=False)
    surname = db.Column(db.String(30), nullable=False)
    name = db.Column(db.String(30), nullable=False)
    medicid = db.Column(db.String(20), unique=True, nullable=False, primary_key=True)
    username = db.Column(db.String(15), unique=True, nullable=False)
    password = db.Column(db.String(15), nullable=False)
    email = db.Column(db.String(50), unique=True, nullable=False)

    # getting the medic ID and using it as the user_id in the load_user function
    def get_id(self):
        return self.medicid

    def __repr__(self):
        return f"Users('{self.position}, {self.surname}, {self.name}, {self.medicid}, {self.username}, {self.email},')"


"""Creating a custom date and time function to deal with the sqlachelmy error of not receiving python date object"""


# class MyDateTime(db.TypeDecorator):
#   impl = db.DateTime

#  def process_bind_param(self, value, dialect):
#     if type(value) is str:
#        return datetime.datetime.strptime(value, "%d/%m/%Y")
#   return value


# End of Creating a custom date and time function to deal with the sqlachelmy error of not receiving python date object


class Inventory(db.Model):
    drugNumber = db.Column(db.String(10), unique=True, nullable=False, primary_key=True)
    drugName = db.Column(db.String(30), unique=True, nullable=False)
    transactionDate = db.Column(db.Date(), nullable=False)
    unitOfMeasure = db.Column(db.String(10), nullable=False)
    unitCost = db.Column(db.String(10), nullable=False)
    unitPrice = db.Column(db.String(10), nullable=False)
    stockType = db.Column(db.String(30), nullable=False)
    quantity = db.Column(db.String(10), nullable=False)
    details = db.Column(db.String(500), nullable=False)
    batchNumber = db.Column(db.String(10), nullable=False)
    expiryDate = db.Column(db.Date(), nullable=False)
    checkedBy = db.Column(db.String(30), nullable=False)

    def __repr__(self):
        return f"Inventory('{self.drugNumber}, {self.drugName}, {self.transactionDate}, {self.unitOfMeasure}, {self.unitCost}, {self.unitPrice}, {self.stockType}, {self.quantity}, {self.details}, {self.batchNumber}, {self.expiryDate}, {self.checkedBy}, ')"


class Patients(db.Model):
    patientID = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    surname = db.Column(db.String(20), nullable=False)
    name = db.Column(db.String(20), nullable=False)
    gender = db.Column(db.String(10), nullable=False)
    dateOfBirth = db.Column(db.Date(), nullable=False)
    age = db.Column(db.Integer, nullable=False)
    contact = db.Column(db.String(10), nullable=False)
    townVillage = db.Column(db.String(20), nullable=False)
    district = db.Column(db.String(20), nullable=False)
    email = db.Column(db.String(50), unique=True, nullable=True)
    guardianName = db.Column(db.String(50), nullable=True)
    guardianContact = db.Column(db.String(10), nullable=True)
    relationship = db.Column(db.String(15), nullable=True)

    """def __init__(
        self,
        patientID,
        surname,
        name,
        gender,
        dateOfBirth,
        contact,
        townVillage,
        district,
        email,
        guardianName,
        guardianContact,
        relationship,
        currentDrug,
        dateOfNextDrug,
    ):
        self.patientID = patientID
        self.surname = surname
        self.name = name
        self.gender = gender
        self.dateOfBirth = dateOfBirth
        self.contact = contact
        self.townVillage = townVillage
        self.district = district
        self.email = email
        self.guardianName = guardianName
        self.guardianContact = guardianContact
        self.relationship = relationship
        self.currentDrug = currentDrug
        self.dateOfNextDrug = dateOfNextDrug"""

    def __repr__(self):
        return f"Patients('{self.patientID}, {self.surname}, {self.name}, {self.gender}, {self.dateOfBirth}, {self.contact}, {self.townVillage}, {self.district}, , {self.email}, {self.guardianName}, {self.guardianContact}, {self.relationship}, {self.currentDrug}, {self.dateOfNextDrug}')"


# service providers' model
class Providers(db.Model):
    providerID = db.Column(db.String(10), unique=True, nullable=False, primary_key=True)
    name = db.Column(db.String(20), nullable=False)
    location = db.Column(db.String(10), nullable=False)
    email = db.Column(db.String(10), nullable=False)
    contact = db.Column(db.String(10), nullable=False)
    address = db.Column(db.String(10), nullable=False)

    def __repr__(self):
        return f"Providers('{self.providerID}, {self.name}, {self.location}, {self.email}, {self.contact}, {self.address},')"


# drugs taken model
class drugs_taken(db.model):
    patientID
    currentDrug = db.Column(db.String(150), nullable=False)
    currentDrug2 = db.Column(db.String(150), nullable=False)
    currentDrug3 = db.Column(db.String(150), nullable=False)
    dateOfNextDrug = db.Column(db.Date(), nullable=False)

    def __repr__(self):
        return f"drugs_taken('{self.patientID}, {self.surname}, {self.name}, {self.gender}, {self.dateOfBirth}, {self.contact}, {self.townVillage}, {self.district}, , {self.email}, {self.guardianName}, {self.guardianContact}, {self.relationship}, {self.currentDrug}, {self.dateOfNextDrug}')"
