from flask import Flask
from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    DateField,
    PasswordField,
    SubmitField,
    TextAreaField,
    BooleanField,
    SelectField,
    SelectMultipleField,
    validators,
)
from wtforms.validators import (
    Email,
    EqualTo,
    Length,
    DataRequired,
    email_validator,
    ValidationError,
    Regexp,
)
from wtforms.fields.html5 import DateField
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from .models import *

"""Signup new user into the database"""


class SignUpForm(FlaskForm):
    position = StringField(
        "Position",
        validators=[DataRequired(), Length(max=20),],
        render_kw={"placeholder": "Position you hold"},
    )
    surname = StringField(
        "Surname",
        validators=[DataRequired(), Length(max=30),],
        render_kw={"placeholder": "Your Surname"},
    )
    name = StringField(
        "Name",
        validators=[DataRequired(), Length(max=30),],
        render_kw={"placeholder": "Your Name"},
    )
    medicid = StringField(
        "Medic id",
        validators=[
            DataRequired(),
            Length(max=20),
            Regexp("^[0-9]*$", message="Only numbers are allowed for this field"),
        ],
        render_kw={"placeholder": "Your ID"},
    )
    username = StringField(
        "Username",
        validators=[DataRequired(), Length(min=3, max=15),],
        render_kw={"placeholder": "Your Username"},
    )
    password = PasswordField(
        "Password",
        validators=[DataRequired(), Length(min=8, max=15),],
        render_kw={"placeholder": "Your Password"},
    )
    repeatpassword = PasswordField(
        "Repeat Password",
        validators=[DataRequired(), EqualTo("password")],
        render_kw={"placeholder": "Confirm Password"},
    )
    email = StringField(
        "Email",
        validators=[DataRequired(), Email()],
        render_kw={"placeholder": "Your Email Address"},
    )
    submit = SubmitField("Sign Up")

    def validate_username(self, username):
        user = Users.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError("Username already taken. Choose another one please")

    def validate_email(self, email):
        user = Users.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError("Email already taken. Choose another one please")


"""Log user into the system"""


class LoginForm(FlaskForm):
    email = StringField(
        "Email",
        validators=[DataRequired(), Email()],
        render_kw={"placeholder": "Your Login Email"},
    )
    password = PasswordField(
        "Password",
        validators=[DataRequired(), Length(min=8, max=15)],
        render_kw={"placeholder": "Your Password"},
    )
    submit = SubmitField("Login")


"""Add drug in inventoty into the database"""


class AddDrugForm(FlaskForm):
    drugNumber = StringField(
        "Drug Number",
        validators=[
            DataRequired(),
            Length(max=20),
            Regexp("^[0-9]*$", message="Only numbers are allowed for this field"),
        ],
        render_kw={"placeholder": "Drug serial number"},
    )
    drugName = StringField(
        "Drug Name",
        validators=[
            DataRequired(),
            Length(max=30),
            Regexp("^[a-zA-Z-_ ]+$", message="Only alphabetical letters are allowed"),
        ],
        render_kw={"placeholder": "Name of drug"},
    )
    transactionDate = DateField("Transaction Date", validators=[DataRequired(),])
    unitOfMeasure = SelectField(
        "Unit of Measure",
        validators=[DataRequired(),],
        choices=[
            ("kilogram", "KG"),
            ("grams", "G"),
            ("milligrams", "MG"),
            ("micrograms", "MCG"),
            ("litre", "L"),
            ("millilitre", "ML"),
            ("cubic centimtre", "CC"),
        ],
    )
    unitCost = StringField(
        "Cost per Unit of this drug",
        validators=[
            DataRequired(),
            Length(max=10),
            Regexp("^[0-9]*$", message="Only numbers are allowed for this field"),
        ],
        render_kw={"placeholder": "Enter cost per unit of this drug (shillings)"},
    )
    unitPrice = StringField(
        "Price per Unit of this drug",
        validators=[
            DataRequired(),
            Length(max=10),
            Regexp("^[0-9]*$", message="Only numbers are allowed for this field"),
        ],
        render_kw={
            "placeholder": "Enter the preregistered unit price for this drug (shillings)"
        },
    )
    stockType = SelectField(
        "Register the type of stock of this drug",
        validators=[DataRequired(),],
        choices=[
            ("nuc", "Nucleoside reverse transcriptase inhibitors"),
            ("non-nuc", "Non-nucleoside reverse transcriptase inhibitors"),
            ("pro", "Protease inhibitors"),
            ("fus", "Fusion inhibitors"),
            ("ccr", "CCR5 antagonists"),
            ("int", "Integrase strand transfer inhibitors"),
            ("pos", "Post-attachment inhibitors"),
            ("pha", "Pharmacokinetic Enhancers"),
            ("com", "Combination HIV medicines"),
        ],
    )
    quantity = StringField(
        "Quantity of the drug being recorded",
        validators=[
            DataRequired(),
            Length(max=10),
            Regexp("^[0-9]*$", message="Only numbers required for this field"),
        ],
        render_kw={"placeholder": "Enter quantity for this drug"},
    )
    details = TextAreaField(
        "Details specific to the drug batch ",
        validators=[DataRequired(), Length(max=1000)],
        render_kw={
            "placeholder": "Enter details specific to the drug batch being added to the inventory. For example, name of the drug supplier or an explanation of why the assigned stock type has been tagged as 'received' versus 'issued.'"
        },
    )
    batchNumber = StringField(
        "Batch Number ",
        validators=[DataRequired(), Length(max=10)],
        render_kw={"placeholder": "Specific batch number for the drug being received."},
    )
    expiryDate = DateField("Expiry Date ", validators=[DataRequired(),])
    checkedBy = StringField(
        "Checked By",
        validators=[DataRequired(message="Field must not be empty"),],
        render_kw={"placeholder": "ID of the admin registering the drug"},
    )
    submit = SubmitField("Add Drug into Inventory")

    def validate_drugNumber(self, drugNumber):
        drug = Inventory.query.filter_by(drugNumber=drugNumber.data).first()
        if drug:
            raise ValidationError(
                "Drug with this number is already registered in the database."
            )

    def validate_batchNumber(self, batchNumber):
        drug = Inventory.query.filter_by(batchNumber=batchNumber.data).first()
        if drug:
            raise ValidationError(
                "This batch number already has a registered drug under it"
            )


"""Add patients and their drugs taken into the database"""


def AddPatientInfo():
    return Inventory.query


def AddPatientInfo2():
    return Inventory.query


def AddPatientInfo3():
    return Inventory.query


# def get_pk(obj):
# return str()


class AddPatientInfoForm(FlaskForm):
    patientID = StringField(
        "Position",
        validators=[DataRequired(), Length(max=20),],
        render_kw={"placeholder": "Patient's identification number"},
    )
    surname = StringField(
        "Surname",
        validators=[DataRequired(), Length(max=30),],
        render_kw={"placeholder": "Patient's surname"},
    )
    name = StringField(
        "Name",
        validators=[DataRequired(), Length(max=30),],
        render_kw={"placeholder": "Patient's Name"},
    )
    gender = SelectField(
        "Gender",
        choices=[("Male", "Male"), ("Female", "Female")],
        validators=[DataRequired(),],
    )
    dateOfBirth = DateField(validators=[DataRequired(),],)
    age = StringField(
        "Age of Patient", validators=[], render_kw={"placeholder": "Patient's Age"},
    )
    contact = StringField(
        "PhoneContact",
        validators=[
            DataRequired(),
            Length(min=10, max=10),
            Regexp("^[0-9]*$", message="Only numbers are allowed for this field"),
        ],
        render_kw={"placeholder": "Patients's Phone number"},
    )
    townVillage = StringField(
        "TownVillage",
        validators=[DataRequired(),],
        render_kw={"placeholder": "Patient's town or village"},
    )
    district = StringField(
        "District",
        validators=[DataRequired(),],
        render_kw={"placeholder": "Patient's district"},
    )
    email = StringField(
        "Email",
        validators=[DataRequired(), Email()],
        render_kw={"placeholder": "Patient's Email Address"},
    )
    guardianName = StringField(
        "Guardian's Name",
        validators=[],
        render_kw={"placeholder": "Patient's Guardian's Name"},
    )
    guardianContact = StringField(
        "Guardian's Contact",
        validators=[],
        render_kw={"placeholder": "Patient's Guardian's Contact"},
    )
    relationship = StringField(
        "Relationship",
        validators=[],
        render_kw={"placeholder": "Relationship with Patient"},
    )
    currentDrug = QuerySelectField(
        "Select drugs",
        query_factory=AddPatientInfo,
        allow_blank=True,
        # get_pk=get_pk,
        get_label="drugName",
    )
    currentDrug2 = QuerySelectField(
        "Select drugs",
        query_factory=AddPatientInfo2,
        allow_blank=True,
        # get_pk=get_pk,
        get_label="drugName",
    )
    currentDrug3 = QuerySelectField(
        "Select drugs",
        query_factory=AddPatientInfo3,
        allow_blank=True,
        # get_pk=get_pk,
        get_label="drugName",
    )

    # dateOfLastDrug = DateField(validators=[DataRequired(),],)
    dateOfNextDrug = DateField(validators=[DataRequired(),],)
    submit = SubmitField("Submit Patient Information into Database")

    def validate_patientID(self, patientID):
        user = Patients.query.filter_by(patientID=patientID.data).first()
        if user:
            raise ValidationError("Patient with this patient ID already exists.")
