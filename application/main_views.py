from flask import (
    Flask,
    render_template,
    redirect,
    url_for,
    request,
    session,
    flash,
    jsonify,
)
from flask_login import login_user, logout_user, current_user, login_required
from application import app, db, bcrypt
from .forms import *
from .models import *
from io import TextIOWrapper
import csv, datetime, time
from datetime import date


@app.route("/dashboard", methods=["GET", "POST"])
@login_required
def dashboard():
    NTRIs = Inventory.query.filter_by(stockType="nuc").count()
    NNTRIs = Inventory.query.filter_by(stockType="non-nuc").count()
    PIs = Inventory.query.filter_by(stockType="pro").count()
    FIs = Inventory.query.filter_by(stockType="fus").count()
    CCR5 = Inventory.query.filter_by(stockType="ccr").count()
    INSTIs = Inventory.query.filter_by(stockType="int").count()
    PaIs = Inventory.query.filter_by(stockType="pos").count()
    PEs = Inventory.query.filter_by(stockType="pha").count()
    CHMs = Inventory.query.filter_by(stockType="com").count()
    # # calculate the age groups for the bar chart, getting the ages of each patient and then categorising them
    """age_ = Patients.query.filter_by(age <= 10).count()
    Age0_10 = Patients.query.filter_by(age <= 10).count()
    Age11_20 = Patients.query.filter_by(age <= 10).count()
    Age21_30 = Patients.query.filter_by(age <= 10).count()
    Age31_40 = Patients.query.filter_by(age <= 10).count()
    Age41_50 = Patients.query.filter_by(age <= 10).count()
    Age51_60 = Patients.query.filter_by(age <= 10).count()
    Age61_ = Patients.query.filter_by(age <= 10).count()"""
    return render_template(
        "main_pages/dashboard.html",
        dcat1=NTRIs,
        dcat2=NNTRIs,
        dcat3=PIs,
        dcat4=FIs,
        dcat5=CCR5,
        dcat6=INSTIs,
        dcat7=PaIs,
        dcat8=PEs,
        dcat9=CHMs,
        patients=Patients.query.count(),
        # age=Age0_10,
    )


@app.route("/inventory", methods=["GET", "POST"])
@login_required
def inventory():
    all_data2 = Inventory.query.all()
    count_data = Inventory.query.count()
    NTRIs = Inventory.query.filter_by(stockType="nuc").all()
    NNTRIs = Inventory.query.filter_by(stockType="non-nuc").all()
    PIs = Inventory.query.filter_by(stockType="pro").all()
    FIs = Inventory.query.filter_by(stockType="fus").all()
    CCR5 = Inventory.query.filter_by(stockType="ccr").all()
    INSTIs = Inventory.query.filter_by(stockType="int").all()
    PaIs = Inventory.query.filter_by(stockType="pos").all()
    PEs = Inventory.query.filter_by(stockType="pha").all()
    CHMs = Inventory.query.filter_by(stockType="com").all()
    form = AddDrugForm()
    if form.validate_on_submit():
        inventory = Inventory(
            drugNumber=form.drugNumber.data,
            drugName=form.drugName.data,
            transactionDate=form.transactionDate.data,
            unitOfMeasure=form.unitOfMeasure.data,
            unitCost=form.unitCost.data,
            unitPrice=form.unitPrice.data,
            stockType=form.stockType.data,
            quantity=form.quantity.data,
            details=form.details.data,
            batchNumber=form.batchNumber.data,
            expiryDate=form.expiryDate.data,
            checkedBy=form.checkedBy.data,
        )
        db.session.add(inventory)
        db.session.commit()
        flash(
            "New drug record has been stored into the database", "success",
        )
        return redirect(url_for("inventory"))
    return render_template(
        "main_pages/inventory.html",
        inventory=all_data2,
        dcat1=NTRIs,
        dcat2=NNTRIs,
        dcat3=PIs,
        dcat4=FIs,
        dcat5=CCR5,
        dcat6=INSTIs,
        dcat7=PaIs,
        dcat8=PEs,
        dcat9=CHMs,
        form=form,
    )


@app.route("/upload_csv", methods=["GET", "POST"])
@login_required
def upload_csv():
    if request.method == "POST":
        csv_file = request.files["csv"]
        csv_file = TextIOWrapper(csv_file)  # , encoding="utf-8"
        csv_reader = csv.reader(csv_file, delimiter=",")
        for row in csv_reader:
            drug = Inventory(
                drugNumber=row[0],
                drugName=row[1],
                transactionDate=datetime.datetime.strptime(row[2], "%Y-%m-%d"),
                unitOfMeasure=row[3],
                unitCost=row[4],
                unitPrice=row[5],
                stockType=row[6],
                quantity=row[7],
                details=row[8],
                batchNumber=row[9],
                expiryDate=datetime.datetime.strptime(row[10], "%Y-%m-%d"),
                checkedBy=row[11],
            )
            db.session.add(drug)
            db.session.commit()
        flash("Drug information from file has been recorded into the system")
        return redirect(url_for("inventory"))
    else:
        return "The CSV is not being loaded or read properly"


@app.route("/providers")
@login_required
def providers():
    return render_template("main_pages/providers.html")


@app.route("/patient_info", methods=["GET", "POST"])
@login_required
def patient_info():
    patients = Patients.query.all()
    form = AddPatientInfoForm()
    if form.validate_on_submit():
        patients = Patients(
            patientID=form.patientID.data,
            surname=form.surname.data,
            name=form.name.data,
            gender=form.gender.data,
            dateOfBirth=form.dateOfBirth.data,
            age=form.age.data,
            contact=form.contact.data,
            townVillage=form.townVillage.data,
            district=form.district.data,
            email=form.email.data,
            guardianName=form.guardianName.data,
            guardianContact=form.guardianContact.data,
            relationship=form.relationship.data,
            currentDrug=form.currentDrug.data.drugName,
            currentDrug2=form.currentDrug2.data.drugName,
            currentDrug3=form.currentDrug3.data.drugName,
            dateOfNextDrug=form.dateOfNextDrug.data,
        )
        db.session.add(patients)
        db.session.commit()
        flash(
            "Records about patient have been saved in the system", "success",
        )
        return redirect(url_for("patient_info"))
    return render_template("main_pages/patient_info.html", form=form, patients=patients)
