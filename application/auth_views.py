from flask import Flask, render_template, redirect, url_for, request, session, flash
from flask_login import login_user, logout_user, current_user
from application import app, db, bcrypt
from .forms import SignUpForm, LoginForm
from .models import *


@app.route("/")
def landing():
    return render_template("authenticate/landing.html")


@app.route("/login/", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = Users.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user)
            next_page = request.args.get("next")
            flash(
                "Welcome!. You have been logged into the system", "success",
            )
            return redirect(next_page) if next_page else redirect(url_for("dashboard"))
        else:
            flash(
                "Login credentials incorrect. Please check email or password", "danger",
            )

    return render_template("authenticate/login.html", form=form)


@app.route("/signup/", methods=["GET", "POST"])
def signup():
    form = SignUpForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode(
            "utf-8"
        )
        user = Users(
            position=form.position.data,
            surname=form.surname.data,
            name=form.name.data,
            medicid=form.medicid.data,
            username=form.username.data,
            password=hashed_password,
            email=form.email.data,
        )
        db.session.add(user)
        db.session.commit()
        flash(
            "Your account has been created and ready for usage", "success",
        )
        return redirect(url_for("landing"))
    return render_template("authenticate/signup.html", form=form)


@app.route("/logout/")
def logout():
    logout_user()
    return redirect(url_for("login"))
