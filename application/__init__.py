from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bcrypt import Bcrypt


app = Flask(__name__, template_folder="templates")
app.config["SECRET_KEY"] = "11606633055e40cbe882f08f27fec5c2501f8978"
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///hiv_didmis.db"

db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = "login"


from application import auth_views
from application import main_views
